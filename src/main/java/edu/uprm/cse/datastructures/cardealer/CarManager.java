package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;       

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;       

@Path("/cars")
public class CarManager {

	private final CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();

	//Returns all car objects in the list
	//URI:http://localhost:8080/cardealer/cars
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] carArr = new Car[cList.size()];
		for (int i = 0; i < cList.size(); i++) {
			carArr[i] = cList.get(i);
		}
		return carArr;
	}

	//Checks for ID of car object, returns if car is in list, and NotFound Exception otherwise.
	//URI:http://localhost:8080/cardealer/cars/{id}
	@GET
	@Path("{id}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		for (int i = 0; i < cList.size(); i++) {
			if(cList.get(i).getCarId() == id) {
				return cList.get(i);
			}
		}
		throw new NotFoundException(new JsonError("Error", "Car" + id + "not found"));
	}                           

	// Adds car object to the list, if car object with the same ID is already present in list then it returns a 406 error status.
	//URI:http://localhost:8080/cardealer/add
	@POST
	@Path("/add") 
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		for (int i = 0;i < cList.size(); i++) {
			if(cList.get(i).getCarId() == car.getCarId()) {
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
				
			}
		}
		cList.add(car);
		return Response.status(201).build();
	}
	
	//Updates the ID of a car object in the list via an ID. Returns a 404 error if no such ID is present in the list;
	//URI:http://localhost:8080/cardealer/cars/{id}/update
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		for (int i = 0; i < cList.size(); i++) {
			if(cList.get(i).getCarId() == car.getCarId()) {
				cList.remove(i);
				cList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	//Removes a car object from the list via an ID, if no such car object exists in the list it returns a 404 error. 
	//URI:http://localhost:8080/cardealer/cars/{id}/delete
	@DELETE
	@Path("{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for (int i = 0; i < cList.size(); i++) {
			if(cList.get(i).getCarId() == id) {
				cList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}                   

	//Defines the NotFoundException error.
	public class NotFoundException extends WebApplicationException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Create a HTTP 404 (Not Found) exception.
		 */
		public NotFoundException() {
			super(Response.status(Response.Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).build());
		}

		/**
		 * Create a HTTP 404 (Not Found) exception.
		 * @param message the String that is the entity of the 404 response.
		 */
		public NotFoundException(String message) {
			super(Response.status(Response.Status.NOT_FOUND).entity(message).type(MediaType.TEXT_PLAIN).build());
		}

		public NotFoundException(JsonError jse) {
			super(Response.status(Response.Status.NOT_FOUND).entity(jse).type(MediaType.APPLICATION_JSON).build());
		}

	}
	
	//Defines the JsonError method.
	public class JsonError {
		private String type;
		private String message;

		public JsonError(String type, String message){
			this.type = type;
			this.message = message;        
		}

		public String getType(){
			return this.type;
		}

		public String getMessage(){
			return this.message;
		}
	}

}

