package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	
	//Creates a CircularSortedDoublyLinkedList in which the car objects can be added and modified.
	private static CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<>(new CarComparator());
	
	//Returns an instance of the car list used to store the car objects.
	public static CircularSortedDoublyLinkedList<Car> getInstance(){		
		return cList;
	}
	
	//Resets the car list when called, used for testing purposes.
	public static CircularSortedDoublyLinkedList<Car> resetCars() {
		cList = new CircularSortedDoublyLinkedList<>(new CarComparator());
		return cList;
	}
 
}



