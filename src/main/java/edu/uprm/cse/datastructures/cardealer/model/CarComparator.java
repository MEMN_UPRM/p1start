package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

//Compares two car objects with priority: CarBrand > CarModel > CarModelOption
public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car car1, Car car2) {
		if(car1.getCarBrand().toString().compareTo(car2.getCarBrand().toString()) < 0) {
			return -1;
		}
		if(car1.getCarBrand().toString().compareTo(car2.getCarBrand().toString()) > 0) {
			return 1;
		}
		if(car1.getCarBrand().toString().compareTo(car2.getCarBrand().toString()) == 0) {
			if(car1.getCarModel().toString().compareTo(car2.getCarModel().toString()) < 0) {
				return -1;
			}
			if(car1.getCarModel().toString().compareTo(car2.getCarModel().toString()) > 0) {
				return 1;
			}
			if(car1.getCarModel().toString().compareTo(car2.getCarModel().toString()) == 0) {
				if(car1.getCarModelOption().toString().compareTo(car2.getCarModelOption().toString()) < 0) {
					return -1;
				}
				if(car1.getCarModelOption().toString().compareTo(car2.getCarModelOption().toString()) > 0) {
					return 1;
				}
			}
		}
		return 0;

	}
}



