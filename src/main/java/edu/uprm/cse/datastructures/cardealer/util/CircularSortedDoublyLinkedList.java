package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E> {
		private Node<E> nextNode;
		private Node<E> prevNode;

		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
			this.prevNode = (Node<E>) header.getPrev();
		}

		@Override
		public boolean hasNext() {
			return nextNode != null;
		}

		private boolean hasPrev() {
			return prevNode != null;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			if (this.hasPrev()) {
				E result = this.prevNode.getElement();
				this.prevNode = this.prevNode.getPrev();
				return result;
			} else {
				throw new NoSuchElementException();
			}
		}
	}

	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}

		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}

	private Node<E> header;
	private int currentSize;
	private Comparator<E> Comp;

	public CircularSortedDoublyLinkedList(Comparator<E> K) {
		this.header = new Node<>();
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.currentSize = 0;
		this.Comp = Comp;
	}

	public static class DefaultComparator<E> implements Comparator<E> {
		public int compare(E o1, E o2) {
			try {
				return ((Comparable<E>) o1).compareTo(o2);
			} catch (ClassCastException e) {
				throw new IllegalArgumentException("Instantiated data type must be Comparable");
			}
		}
	}

	@Override
	public boolean add(E obj) {

		if (this.isEmpty()) {
			Node<E> newNode = new Node<E>(obj, this.header, this.header);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			this.currentSize++;
			return true;
		}
		for (Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext()) {
			
			if (Comp.compare(temp.getElement(), obj) > 0) {
				Node<E> obj2 = new Node<>(obj, null, null);

				obj2.setNext(temp);
				obj2.setPrev(temp.getPrev());
				temp.getPrev().setNext(obj2);
				temp.setPrev(obj2);
				this.currentSize++;
				return true;
			} 
			
			if(temp.getNext()==this.header) {
				Node<E> obj2 = new Node<>(obj, null, null);
				
				obj2.setNext(this.header);
				obj2.setPrev(this.header.getPrev());
				this.header.getPrev().setNext(obj2);
				this.header.setPrev(obj2);
				this.currentSize++;
				return true;
			}
		}
		return false;
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);
		if (i < 0) {
			return false;
		} else {
			this.remove(i);
			return true;
		}
	}

	@Override
	public boolean remove(int index) {
		if ((index < 0) || (index >= this.currentSize)) {
			throw new IndexOutOfBoundsException();
		} else {
			Node<E> temp = this.header;
			int currentPosition = 0;
			Node<E> target = null;

			while (currentPosition != index) {
				temp = temp.getNext();
				currentPosition++;
			}
			E result = null;
			target = temp.getNext();
			result = target.getElement();
			temp.setNext(target.getNext());
			target.getNext().setPrev(temp);

			target.setElement(null);
			target.setNext(null);
			target.setPrev(null);
			this.currentSize--;
			return true;

		}
	}

	@Override
	public int removeAll(E obj) {
		int count = 0;
		while (this.remove(obj)) {
			count++;
		}
		return count;
	}

	@Override
	public void clear() {
		while (!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public int firstIndex(E e) {
		int i = 0;
		for (Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext(), ++i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int i = this.currentSize-1;
		for (Node<E> temp = this.header.getPrev(); temp != this.header; temp = temp.getPrev(), --i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public E first() {
		return (this.isEmpty() ? null : this.header.getNext().getElement());
	}

	@Override
	public E last() {
		if (this.isEmpty()) {
			return null;
		}
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		if ((index < 0) || index >= this.currentSize) {
			throw new IndexOutOfBoundsException();
		}

		Node<E> temp = this.getPosition(index);
		return temp.getElement();

	}

	private Node<E> getPosition(int index) {
		int currentPosition = 0;
		Node<E> temp = this.header.getNext();

		while (currentPosition != index) {
			temp = temp.getNext();
			currentPosition++;
		}
		return temp;

	}

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();

	}

	public CircularSortedDoublyLinkedList<E> getInstance() {
		return new CircularSortedDoublyLinkedList<E>(Comp);
	}

}
